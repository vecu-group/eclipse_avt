package org.da.vecu.drimcave.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;

public class ServerConnectionThread extends Thread {
	private final int PORT = 11190;
	public static ServerSocket serverSocket = null;
	public static boolean isRunServer = false;
	private int clientCount = 0;
	private static Vector<Socket> socketList = new Vector<Socket>();

	public ServerConnectionThread() {
		try {
			serverSocket = new ServerSocket(PORT);
			ViewManager.Print2(PrintType.Info, "[Server] TCP Server started on port 11190");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		isRunServer = true;
		while (isRunServer) {
			try {
				ViewManager.Print2(PrintType.Info, "[Server] Run Connection Manager.");
				Socket socket = serverSocket.accept();
				ServerProcessThread thread = new ServerProcessThread(socket, clientCount);
				socketList.add(socket);
				ViewManager.Print2(PrintType.Info, "[Server] Accepted client");
				thread.start();
				clientCount++;
			} catch (IOException e) {
				ViewManager.Print2(PrintType.Info, "[Server] Exit Connection Manager.");
				isRunServer = false;
			}
		}
	}

	public static void Close() {
		if (isRunServer == false)
			return;
		isRunServer = false;
		try {

			for (int i = 0; i < socketList.size(); i++) {
				Socket s = socketList.get(i);
				if (s != null) {
					if (s.isConnected())
						s.close();
				}
			}

			if (serverSocket != null) {
				serverSocket.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
