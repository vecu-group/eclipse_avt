package org.da.vecu.drimcave.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;

public class ServerProcessThread extends Thread {

	Socket socket;
	int id;
	byte[] buffer = new byte[1024];

	public ServerProcessThread(Socket socket, int id) {
		this.socket = socket;
		this.id = id;
	}

	@Override
	public void run() {
		try {
			while (true) {
				InputStream IS = socket.getInputStream();
				int size = IS.read(buffer);
				if (size < 0) {
					ViewManager.Print2(PrintType.Info, "[Server] Client connection terminated.");
					break;
				}
				String output = new String(buffer, 0, size, "UTF-8");
				ConnectionMgr.processPacket(output, socket);
			}
		} catch (IOException e) {
			ViewManager.Print2(PrintType.Info, "[Server] Client connection terminated..");
		}
	}

}
