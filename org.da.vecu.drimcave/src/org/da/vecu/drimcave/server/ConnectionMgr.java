package org.da.vecu.drimcave.server;

import org.da.vecu.drimcave.view.ViewManager;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.da.vecu.drimcave.editors.GridLayoutTestcasePage;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;

public class ConnectionMgr {
	public static Socket socketAsamxil;
	public static Socket socketWrapper;
	public static Socket socketConnMgrClient;

	enum ClientType {
		WRAPPER(0), ASAMDLL(1), CONNMGRCLIENT(2);

		private final int value;

		private ClientType(int v) {
			value = v;
		}

		public int toInt() {
			return value;
		}
	}

	enum MsgType {
		NONE(0), PRINT(1), STATUS(2);

		private final int value;

		private MsgType(int v) {
			value = v;
		}

		public int toInt() {
			return value;
		}
	}

	enum DataType {
		NONE(0), STRING(1), STATUS(2);

		private final int value;

		private DataType(int v) {
			value = v;
		}

		public int toInt() {
			return value;
		}
	}

	enum StatusType {
		NONE(0), TEST1(1), TEST2(2);

		private final int value;

		private StatusType(int v) {
			value = v;
		}

		public int toInt() {
			return value;
		}
	}

	public class ConnData {
	}

	public static void processPacket(String packet, Socket soc) {
		// 0,2,2,1;
		// 0,1,1,text;

		String[] pacTok = packet.split(";");
		for (int i = 0; i < pacTok.length; i++) {
			String[] t = pacTok[i].split(",");
			if (t.length != 4)
				continue;
			ViewManager.Print2(PrintType.Debug, pacTok[i]);
			initSocekt(t[0], soc);
			process(t[0], t[1], t[2], t[3]);
		}
	}

	private static void process(String sId, String sMsgType, String sDatType, String data) {
		int id = Integer.parseInt(sId);
		int msgType = Integer.parseInt(sMsgType);
		int dataType = Integer.parseInt(sDatType);

		if (msgType == 1) {
			ViewManager.Print2(PrintType.Info, data);
		}

		// ASAM XIL DLL > AVT
		if (id == ClientType.ASAMDLL.toInt() && msgType == MsgType.STATUS.toInt()
				&& dataType == DataType.STATUS.toInt()) {
			int num = Integer.parseInt(data);
			if (num == 1) { // 1,2,2,1;
				ViewManager.Print2(PrintType.Info, "[Server] Start the simulation in an external tool.(Rx 1,2,2,1)");
				GridLayoutTestcasePage.RunMasterSim();
			}
		}

		// WRAPPER > AVT
		if (id == ClientType.WRAPPER.toInt() && msgType == MsgType.STATUS.toInt()
				&& dataType == DataType.STATUS.toInt()) {

			int num = Integer.parseInt(data);
			if (num == 1) { // 0,2,2,1;
				ViewManager.Print2(PrintType.Info, "[Server] FMU is ready to run simulation.(Rx 0,2,2,1)");
				send2AsamXilDll_StartSimulation();
			}
		}
	}

	private static void initSocekt(String sId, Socket soc) {
		int id = Integer.parseInt(sId);
		if (id == 0) {
			socketWrapper = soc;
		}
		if (id == 1) {
			socketAsamxil = soc;
		}
		if (id == 2) {
			socketConnMgrClient = soc;
		}
	}

	private static void send2AsamXilDll_StartSimulation() {
		if (socketAsamxil == null) {
			return;
		}
		if (socketAsamxil.isConnected() == false) {
			ViewManager.Print2(PrintType.Error, "[Server] socketAsamxil.isConnected() false");
			return;
		}
		try {

			String sendMsg = "1,2,2,2;";
			byte[] buf = sendMsg.getBytes("UTF-8");
			OutputStream os = socketAsamxil.getOutputStream();
			os.write(buf);
			ViewManager.Print2(PrintType.Info, "[Server] Notify ASAMXIL of readiness.(Tx 1,2,2,2)");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static void send2ConnMgrClient(String msg) {
		if (socketConnMgrClient == null) {
			return;
		}
		if (socketConnMgrClient.isConnected() == false) {
			ViewManager.Print2(PrintType.Error, "[Server] socketConnMgrClient.isConnected() false");
			return;
		}
		try {
			byte[] buf = msg.getBytes("UTF-8");
			OutputStream os = socketConnMgrClient.getOutputStream();
			os.write(buf);
			ViewManager.Print2(PrintType.Info, String.format("[Server] Avt > ConnMgrClient : CANoe Open.Tx[%s]", msg));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void send2ConnMgrClient_CanoeOpen() {
		send2ConnMgrClient("2,3,2,1;");
	}

	public static void send2ConnMgrClient_CanoeStart() {
		send2ConnMgrClient("2,3,2,2;");
	}

	public static void send2ConnMgrClient_CanoeStop() {
		send2ConnMgrClient("2,3,2,3;");
	}
	
	public static void send2ConnMgrClient_Close() {
		send2ConnMgrClient("2,3,2,255;");
	}
	
	public static void send2ConnMgrClient_MatlabOpen() {
		send2ConnMgrClient("2,3,2,5;");
	}
}
