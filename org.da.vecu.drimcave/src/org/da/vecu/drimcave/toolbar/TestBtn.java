
package org.da.vecu.drimcave.toolbar;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class TestBtn {
	boolean isStart = false;
	ServerSocket serverSocket;
	Socket socket = null;

	class TestWrapperServer extends Thread {
		public TestWrapperServer() {
			try {
				serverSocket = new ServerSocket(11180);
				ViewManager.Print2(PrintType.Debug, "TCP Server started on port 11180");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void run() {
			try {
				if (socket == null) {
					socket = serverSocket.accept();
					ViewManager.Print2(PrintType.Debug, "Test Server(11180)");
				}
			} catch (IOException e) {
				ViewManager.Print2(PrintType.Debug, "Test Server(11180) Error");
				return;
			}

			while (true) {
				InputStream IS;
				try {
					IS = socket.getInputStream();
					byte[] bt = new byte[256];
					int size = IS.read(bt);
					String output = new String(bt, 0, size, "UTF-8");
					ViewManager.Print2(PrintType.Debug, "Test Server(11180) Rx:" + output);
				} catch (IOException e) {
					ViewManager.Print2(PrintType.Debug, "Disconnect Test Server(11180)");
					break;
				}
				// System.out.println("Thread " + id + " > " + output);
			}
		}

		public void Stop() {
			try {
				if (socket != null)
					socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	static TestWrapperServer testServer = null;

	@Execute
	public void execute() {

		if (isStart == false) {

			ViewManager.Print2(PrintType.Debug, "Start the Wrapper Server for Test. (Output received data)");
			isStart = true;
			TestWrapperServer testServer = new TestWrapperServer();
			testServer.start();
		} else {
			isStart = false;
			testServer.Stop();
			// t.Close();
		}
	}

	public static void TestServerClose()
	{
		if(testServer == null)
			return;
		testServer.Stop();
	}
}