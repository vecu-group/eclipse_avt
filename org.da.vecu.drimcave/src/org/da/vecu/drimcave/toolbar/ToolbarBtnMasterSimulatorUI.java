
package org.da.vecu.drimcave.toolbar;


import org.da.vecu.drimcave.Util.CommandExecutor;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class ToolbarBtnMasterSimulatorUI {
	@Execute
	public void execute() {
		String strSuccess = "Run MatserSimulator UI.";
		String strFail = "Failed to Run MasterSimulator UI.";
		
		if (CommandExecutor.RunMasterSimUI()) {
			ViewManager.Print2(PrintType.Info, strSuccess);
		} else {
			ViewManager.Print2(PrintType.Error, strFail);
		}
	}
}