 
package org.da.vecu.drimcave.toolbar;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class TestBtn2 {
	@Execute
	public void execute() {
		/*
		ViewManager.Print2(PrintType.Debug, "Start the Wrapper Client for Test. (TCP IP message sent).");
		
		try {

			Socket socket = new Socket("127.0.0.1", 11190);
			OutputStream output = socket.getOutputStream();
			PrintWriter writer = new PrintWriter(output, true);
			writer.println("0,2,2,1;");
			socket.close();
		} catch (Exception ex) {
			ViewManager.Print2(PrintType.Debug, "Test TcpClient(Wrapper) Exception. :" + ex.getMessage());
		}
		*/
		
		ViewManager.Print2(PrintType.Debug, "Test 11180 TCP Send.");
		Socket socket;
		try {
			socket = new Socket("127.0.0.1", 11180);
			OutputStream output = socket.getOutputStream();
			PrintWriter writer = new PrintWriter(output, true);
			writer.println("0xc8,8,0x00 0x00 0x01 0x01 0xFF 0xFF 0xFF 0xFF");
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ViewManager.Print2(PrintType.Debug, "Failed Test 11180 TCP Send.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ViewManager.Print2(PrintType.Debug, "Failed Test 11180 TCP Send..");
		}
	}
		
}