
package org.da.vecu.drimcave.toolbar;

import org.da.vecu.drimcave.Util.CommandExecutor;
import org.da.vecu.drimcave.Util.ListenerMgr;
import org.da.vecu.drimcave.server.ServerConnectionThread;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.e4.core.di.annotations.Execute;

public class ToolbarBtnConnectionMgr {
	boolean isStart = false;

	@Execute
	public void execute() {
		CommandExecutor.InitChRun();
		ListenerMgr.ActivateListener();
		if (isStart == false) {
			isStart = true;
			ServerConnectionThread server = new ServerConnectionThread();
			server.start();
			ViewManager.Print2(PrintType.Info, "Run ConnectionMgr Server.");
		} else {
			isStart = false;
			ServerConnectionThread.Close();
			ViewManager.Print2(PrintType.Info, "Stop ConnectionMgr Server.");
		}
	}
}