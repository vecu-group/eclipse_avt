 
package org.da.vecu.drimcave.toolbar;

import javax.inject.Named;

import org.da.vecu.drimcave.ConnMgrClient.CmcControlWindow;
import org.da.vecu.drimcave.Util.AvtEnvIniParser;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

public class ToolbarBtnConnectionMgrClient {
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell s) {
		//String path = AvtEnvIniParser.GetIniConnMgrClientPath();
		
		CmcControlWindow wnd = new CmcControlWindow(s);
		wnd.setBlockOnOpen(true);
		wnd.open();		
	}
		
}