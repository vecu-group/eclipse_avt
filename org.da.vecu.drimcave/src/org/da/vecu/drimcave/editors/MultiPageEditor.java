package org.da.vecu.drimcave.editors;

import org.da.vecu.drimcave.Util.CommandExecutor;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.MultiPageEditorPart;

public class MultiPageEditor extends MultiPageEditorPart implements IResourceChangeListener {

	private GridLayoutTestcasePage testcasePage;
	private TextEditor uiTextEditConfig;
	private final int editIndex = 1;

	public MultiPageEditor() {
		super();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
	}

	void createPage1() {
		int index;
		Composite composite2 = new Composite(getContainer(), SWT.NONE);
		testcasePage = new GridLayoutTestcasePage(composite2);
		index = addPage(composite2);
		setPageText(index, "TestCase");
	}

	void createPage2() {
		try {
			uiTextEditConfig = new TextEditor();
			int index = addPage(uiTextEditConfig, getEditorInput());
			// setPageText(index, editor.getTitle());
			setPageText(index, ".dccfg");
		} catch (PartInitException e) {
			ErrorDialog.openError(getSite().getShell(), "Error creating nested text editor", null, e.getStatus());
		}
	}

	@Override
	protected void createPages() {
		// createPage0();
		createPage1();
		createPage2();

		DcConfigData.setDocument(uiTextEditConfig.getDocumentProvider().getDocument(uiTextEditConfig.getEditorInput()));
		testcasePage.InitData(DcConfigData.getAllText());
		CheckMasterSim();
	}

	@Override
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		getEditor(editIndex).doSave(monitor);
	}

	@Override
	public void doSaveAs() {
		IEditorPart editor = getEditor(editIndex);
		editor.doSaveAs();
		setPageText(editIndex, editor.getTitle());
		setInput(editor.getEditorInput());
	}

	public void gotoMarker(IMarker marker) {
		setActivePage(editIndex);
		IDE.gotoMarker(getEditor(editIndex), marker);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		if (!(input instanceof IFileEditorInput))
			throw new PartInitException("Invalid Input: Must be IFileEditorInput");
		super.init(site, input);

		this.setPartName(((FileEditorInput) input).getFile().getName());

	}

	@Override
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		if (event.getType() == IResourceChangeEvent.PRE_CLOSE) {
			Display.getDefault().asyncExec(() -> {
				IWorkbenchPage[] pages = getSite().getWorkbenchWindow().getPages();
				for (IWorkbenchPage page : pages) {
					if (((FileEditorInput) uiTextEditConfig.getEditorInput()).getFile().getProject()
							.equals(event.getResource())) {
						IEditorPart editorPart = page.findEditor(uiTextEditConfig.getEditorInput());
						page.closeEditor(editorPart, true);
					}
				}
			});
		}
	}

	private void CheckMasterSim() {
		String strSuccess = "Ready to run MasterSimulator.";
		String strFail = "MasterSimulator is not ready. Make sure it is installed.";
		if (CommandExecutor.RunCheckMasterSim()) {
			ViewManager.Print2(PrintType.Info, strSuccess);
		} else {
			ViewManager.Print2(PrintType.Error, strFail);
		}
	}
}
