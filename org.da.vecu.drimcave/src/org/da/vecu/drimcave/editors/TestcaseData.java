package org.da.vecu.drimcave.editors;

import java.io.File;

public class TestcaseData {
	public enum TC_RESULT {
		NONE(0, "None"), PASS(1, "Pass"), FAIL(2, "Fail"), N_A(3, "N/A");

		private final int value;
		private final String text;

		private TC_RESULT(int value, String text) {
			this.value = value;
			this.text = text;
		}

		public int toInt() {
			return value;
		}

		public String toString() {
			return text;
		}
	}

	private String name;
	private String msimPath;
	private String inputPath;
	private String expectedPath;
	private TC_RESULT Result;
	private final String INPUT_MERGE_FILE_TOK = "dc_input_";

	public TestcaseData() {
		name = "";
		msimPath = "";
		inputPath = "";
		expectedPath = "";
		Result = TC_RESULT.NONE;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setMsimPath(String path) {
		msimPath = path;
	}

	public String getMsimPath() {
		return msimPath;
	}

	public String getMsimInputPath() {
		File f = new File(msimPath);
		String path = f.getPath();// D:\1_workspace\230727_VirtualECU\2_Drimcave(testtool)\SimulationTest\231114_Test.msim
		String name = f.getName();// 231114_Test.msim
		String onlyPath = path.replace(name, "");// D:\1_workspace\230727_VirtualECU\2_Drimcave(testtool)\SimulationTest\
		String mi = String.format("%s%s%s", onlyPath, INPUT_MERGE_FILE_TOK, name);

		return mi;
	}

	public void setInputPath(String path) {
		inputPath = path;
	}

	public String getInputPath() {
		return inputPath;
	}

	public void setExpectedPath(String path) {
		expectedPath = path;
	}

	public String getExpectedPath() {
		return expectedPath;
	}

	public String getMsimFileNameUiString() {
		File f = new File(msimPath);

		if (f.getName().equals("")) {
			return "...";
		}
		return f.getName();
	}

	public String getInputFileNameUiString() {
		File f = new File(inputPath);

		if (f.getName().equals("")) {
			return "...";
		}
		return f.getName();
	}

	public String getExpectedFileNameUiString() {
		File f = new File(expectedPath);

		if (f.getName().equals("")) {
			return "...";
		}
		return f.getName();
	}

	public String getMsimFileName() {
		File f = new File(msimPath);
		return f.getName();
	}

	public void setResult(TC_RESULT v) {
		Result = v;
	}

	public TC_RESULT getResult() {
		return Result;
	}
}
