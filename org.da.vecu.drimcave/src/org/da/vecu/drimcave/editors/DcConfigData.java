package org.da.vecu.drimcave.editors;


import org.eclipse.jface.text.IDocument;

public class DcConfigData {
	static private IDocument editDoc;

	static void setDocument(IDocument doc)
	{
		editDoc = doc;
		if(editDoc == null)
			return;
	}
	
	static public String getNewDcFileFormat()
	{
		return "";
	}
	
	static public String getAllText()
	{
		if(editDoc == null)
			return "";
		return editDoc.get();
	}
	static public void setDocText(String text)
	{
		if(editDoc == null)
			return ;
		editDoc.set(text);
	}
}
