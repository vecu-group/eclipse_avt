package org.da.vecu.drimcave.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class GridLayoutConfigPage {
	private GridLayout layout;
	private Text uiTextMsimPath;
	private Text uiTextStiPath;
	private Text uiTextConfigView;
	private Composite composite;

	public GridLayoutConfigPage(Composite composite) {
		this.composite = composite;
		layout = new GridLayout();
		composite.setLayout(layout);

		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = false;
		layout.marginHeight = 30;
		layout.marginWidth = 20;
		layout.horizontalSpacing = 20;
		layout.verticalSpacing = 15;

		GridData gdLabel = new GridData(GridData.FILL);
		gdLabel.widthHint = 80;
		GridData gdText = new GridData(GridData.BEGINNING);
		gdText.widthHint = 500;
		GridData gdBtn = new GridData(GridData.CENTER);
		gdBtn.widthHint = 80;
		GridData gdView = new GridData(GridData.BEGINNING);
		gdView.horizontalSpan = 3;
		gdView.widthHint = gdText.widthHint + gdBtn.widthHint + gdLabel.widthHint + layout.horizontalSpacing;
		gdView.heightHint = 250;

		Label uiLabelMsim = new Label(composite, SWT.NONE);
		uiTextMsimPath = new Text(composite, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		Button uiBtnConfigPath = new Button(composite, SWT.NONE);
		uiLabelMsim.setText(".msim file");
		uiTextMsimPath.setLayoutData(gdText);
		uiTextMsimPath.setText("");
		uiBtnConfigPath.setLayoutData(gdBtn);
		uiBtnConfigPath.setText("...");
		uiBtnConfigPath.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// BtnClickMsimPath();
			}
		});

		Label uiLabelSti = new Label(composite, SWT.NONE);
		uiTextStiPath = new Text(composite, SWT.SINGLE | SWT.BORDER | SWT.READ_ONLY);
		Button uiBtnStiPath = new Button(composite, SWT.NONE);
		uiLabelSti.setText(".sti file");
		uiTextStiPath.setLayoutData(gdText);
		uiTextStiPath.setText("");
		uiBtnStiPath.setLayoutData(gdBtn);
		uiBtnStiPath.setText("...");
		uiBtnStiPath.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// BtnClickStiPath();
			}
		});

		uiTextConfigView = new Text(composite, SWT.MULTI | SWT.BORDER | SWT.READ_ONLY);
		uiTextConfigView.setLayoutData(gdView);
	}

	String getFilePath(String ext) {
		FileDialog dialog = new FileDialog(composite.getShell(), SWT.OPEN);
		dialog.setText("Open File");
		String[] filterExt = { ext, "*.*" };
		dialog.setFilterExtensions(filterExt);
		return dialog.open();
	}
}