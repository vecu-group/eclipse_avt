package org.da.vecu.drimcave.editors;

import java.util.Vector;

import org.da.vecu.drimcave.Util.ConfigParser;
import org.da.vecu.drimcave.Util.MasterSimRunner;
import org.da.vecu.drimcave.editors.TestcaseData.TC_RESULT;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class GridLayoutTestcasePage {
	private Composite composite;
	private Table uiTableTestcase;
	private static Button uiBtnRun;
	private Vector<TestcaseData> vTcData = new Vector<>();;
	private boolean isTcNameModify = false;
	private Color[] arrColor = new Color[4];

	private final int WIDTH_LABEL_TITLE = 600;
	private final int WIDTH_BTN_RUN = 100;
	private final int WIDTH_LAYOUT_HOR_SPACING = 20;

	private final int TABLE_COL_INDEX_NUM = 0;
	private final int TABLE_COL_INDEX_NAME = 1;
	private final int TABLE_COL_INDEX_MSIM = 2;
	private final int TABLE_COL_INDEX_INPUT = 3;
	private final int TABLE_COL_INDEX_EXPECTED = 4;
	private final int TABLE_COL_INDEX_RESULT = 5;

	public GridLayoutTestcasePage(Composite composite) {
		this.composite = composite;

		GridLayout layout = CreateLayout();
		composite.setLayout(layout);
		InitResultColor();
		CreateLabelTitle();
		uiBtnRun = CreateBtnRun();
		uiTableTestcase = CreateTable();
	}

	public void InitData(String text) {
		vTcData = ConfigParser.parse(text);
		UpdateUiList();
	}

	GridLayout CreateLayout() {
		GridLayout layout = new GridLayout();

		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;
		layout.marginHeight = 30;
		layout.marginWidth = 20;
		layout.horizontalSpacing = WIDTH_LAYOUT_HOR_SPACING;
		layout.verticalSpacing = 15;

		return layout;
	}

	void InitResultColor() {
		Color greenColor = composite.getShell().getDisplay().getSystemColor(SWT.COLOR_GREEN);
		Color redColor = composite.getShell().getDisplay().getSystemColor(SWT.COLOR_RED);
		Color grayColor = composite.getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY);
		Color whiteColor = composite.getShell().getDisplay().getSystemColor(SWT.COLOR_GRAY);

		arrColor[TestcaseData.TC_RESULT.FAIL.toInt()] = redColor;
		arrColor[TestcaseData.TC_RESULT.PASS.toInt()] = greenColor;
		arrColor[TestcaseData.TC_RESULT.NONE.toInt()] = whiteColor;
		arrColor[TestcaseData.TC_RESULT.N_A.toInt()] = grayColor;
	}

	Label CreateLabelTitle() {
		Label label = new Label(composite, SWT.SINGLE | SWT.READ_ONLY);

		GridData gd = new GridData(GridData.BEGINNING);
		gd.widthHint = WIDTH_LABEL_TITLE;

		label.setLayoutData(gd);
		label.setText("TestCase");
		return label;
	}

	public Button CreateBtnRun() {
	    final String BtnStart = "Run Testcase";
	    final String BtnStop = "Stop Testcase";
	    Button btn = new Button(composite, SWT.NONE);

	    GridData gd = new GridData(GridData.CENTER);
	    gd.widthHint = 115;

	    btn.setLayoutData(gd);
	    btn.setText(BtnStart);

	    final MasterSimRunner[] runner = new MasterSimRunner[1];

	    btn.addSelectionListener(new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	            if (MasterSimRunner.isRun) {
	                // Stop the test
	                if (runner[0] != null) {
	                    runner[0].StopThread();
	                }
	                btn.setText(BtnStart);
	                btn.setEnabled(true);
	            } else {
	                // Start the test
	                MasterSimRunner.MasterSimRunnerCallback callback = new MasterSimRunner.MasterSimRunnerCallback() {
	                    @Override
	                    public void FailedValidFile(int i) {
	                        TestcaseData data = vTcData.get(i);
	                        ViewManager.Print2(PrintType.Error,
	                                String.format("[Testcase] File is invalid. [%s]", data.getMsimPath()));

	                        Display.getDefault().syncExec(() -> {
	                            btn.setText(BtnStart);
	                            btn.setEnabled(true);
	                        });
	                    }

	                    @Override
	                    public void SuccessValidFile() {
	                        ViewManager.Print2(PrintType.Info, "[Testcase] Completed File validation.");
	                    }

	                    @Override
	                    public void FailedInputFile(int i) {
	                        TestcaseData data = vTcData.get(i);
	                        ViewManager.Print2(PrintType.Error,
	                                String.format("[Testcase] Input file is invalid. [%s]", data.getInputPath()));

	                        Display.getDefault().syncExec(() -> {
	                            btn.setText(BtnStart);
	                            btn.setEnabled(true);
	                        });
	                    }

	                    @Override
	                    public void SuccessInputFile() {
	                        ViewManager.Print2(PrintType.Info, "[Testcase] Input data input complete.");
	                    }

	                    @Override
	                    public void StepExeTestcase(int i, TC_RESULT r) {
	                        Display.getDefault().syncExec(() -> SetResultItem(i, r));
	                    }

	                    @Override
	                    public void FailedExeTestcase(int i) {
	                        TestcaseData data = vTcData.get(i);
	                        ViewManager.Print2(PrintType.Error,
	                                String.format("[Testcase] An error occurred during simulation. Abort the test case.[%s]",
	                                        data.getName()));

	                        Display.getDefault().syncExec(() -> {
	                            btn.setText(BtnStart);
	                            btn.setEnabled(true);
	                        });
	                    }

	                    @Override
	                    public void SuccessExeTestcase() {
	                        ViewManager.Print2(PrintType.Info, "[Testcase] Testcase completed.");

	                        Display.getDefault().syncExec(() -> {
	                            btn.setText(BtnStart);
	                            btn.setEnabled(true);
	                        });
	                    }
	                };

	                ResetResultItem();
	                runner[0] = new MasterSimRunner(vTcData, callback);
	                runner[0].start();
	                btn.setText(BtnStop);
	            }
	        }
	    });

	    return btn;
	}

	Table CreateTable() {
		Table tb = new Table(composite, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		TableColumn uiTblColNum = new TableColumn(tb, SWT.NONE);
		TableColumn uiTblColName = new TableColumn(tb, SWT.NONE);
		TableColumn uiTblColMsim = new TableColumn(tb, SWT.NONE);
		TableColumn uiTblColInput = new TableColumn(tb, SWT.NONE);
		TableColumn uiTblColExpected = new TableColumn(tb, SWT.NONE);
		TableColumn uiTblColResult = new TableColumn(tb, SWT.NONE);

		GridData gdTable = new GridData(GridData.BEGINNING);
		gdTable.horizontalSpan = 2;
		gdTable.widthHint = WIDTH_LABEL_TITLE + WIDTH_BTN_RUN + WIDTH_LAYOUT_HOR_SPACING;
		gdTable.heightHint = 250;

		tb.setLayoutData(gdTable);
		tb.setLinesVisible(true);
		tb.setHeaderVisible(true);

		uiTblColNum.setText("#");
		uiTblColName.setText("TestCase");
		uiTblColMsim.setText(".msim");
		uiTblColInput.setText("input");
		uiTblColExpected.setText("expected");
		uiTblColResult.setText("Result");

		uiTblColNum.setWidth(40);
		uiTblColName.setWidth(250);
		uiTblColMsim.setWidth(150);
		uiTblColInput.setWidth(90);
		uiTblColExpected.setWidth(100);
		uiTblColResult.setWidth(85);

		return tb;
	}

	void UpdateUiList() {
		uiTableTestcase.removeAll();
		uiTableTestcase.clearAll();

		for (int i = 0; i < vTcData.size(); i++) {
			TestcaseData data = vTcData.get(i);
			TableItem item = new TableItem(uiTableTestcase, SWT.NONE);

			AddNumBtn(item, data);
			AddTestnameEdit(item, data);
			AddMsimBtn(item, data);
			AddInputBtn(item, data);
			AddExpectedBtn(item, data);
			SetResultItem(i, data.getResult());
			item.setData(data);
			item.addDisposeListener(new DisposeListener() {
				@Override
				public void widgetDisposed(DisposeEvent e) {
					Button col0 = (Button) item.getData("col0");
					Text col1 = (Text) item.getData("col1");
					Button col2 = (Button) item.getData("col2");
					Button col3 = (Button) item.getData("col3");
					Button col4 = (Button) item.getData("col4");

					col0.dispose();
					col1.dispose();
					col2.dispose();
					col3.dispose();
					col4.dispose();
				}
			});
		}
		AddPlusBtn();

	}

	void UpdateConfigContent() {
		String allText = ConfigParser.exportText(vTcData);
		DcConfigData.setDocText(allText);
	}

	void AddNumBtn(TableItem item, TestcaseData data) {
		TableEditor tbEdit = new TableEditor(uiTableTestcase);
		tbEdit.grabHorizontal = true;

		Button btn = new Button(uiTableTestcase, SWT.PUSH);
		btn.setText(Integer.toString(vTcData.indexOf(data)));
		tbEdit.setEditor(btn, item, TABLE_COL_INDEX_NUM);
		item.setData("col0", btn);
		btn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				vTcData.remove(data);
				UpdateUiList();
			}
		});
	}

	void AddTestnameEdit(TableItem item, TestcaseData data) {
		TableEditor tbEdit = new TableEditor(uiTableTestcase);
		tbEdit.grabHorizontal = true;
		Text edit = new Text(uiTableTestcase, SWT.NONE);
		edit.setText(data.getName());
		tbEdit.setEditor(edit, item, TABLE_COL_INDEX_NAME);
		item.setData("col1", edit);

		edit.addListener(SWT.Modify, new Listener() {
			public void handleEvent(Event e) {
				Text text = (Text) e.widget;
				if (text.isFocusControl()) {
					data.setName(text.getText());
					isTcNameModify = true;
				}
			}
		});
		edit.addListener(SWT.FocusOut, new Listener() {
			public void handleEvent(Event e) {
				if (isTcNameModify) {
					isTcNameModify = false;
					UpdateConfigContent();
				}
			}
		});
	}

	void AddMsimBtn(TableItem item, TestcaseData data) {
		TableEditor tbEdit = new TableEditor(uiTableTestcase);
		tbEdit.grabHorizontal = true;

		Button btn = new Button(uiTableTestcase, SWT.PUSH);
		btn.setText(data.getMsimFileNameUiString());
		tbEdit.setEditor(btn, item, TABLE_COL_INDEX_MSIM);
		item.setData("col2", btn);
		btn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String path = getFilePath("*.msim");
				data.setMsimPath(path);
				btn.setText(data.getMsimFileNameUiString());
				UpdateConfigContent();
			}
		});
	}

	void AddInputBtn(TableItem item, TestcaseData data) {
		TableEditor tbEdit = new TableEditor(uiTableTestcase);
		tbEdit.grabHorizontal = true;

		Button btn = new Button(uiTableTestcase, SWT.PUSH);
		btn.setText(data.getInputFileNameUiString());
		tbEdit.setEditor(btn, item, TABLE_COL_INDEX_INPUT);
		item.setData("col3", btn);
		btn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String path = getFilePath("*.csv");
				data.setInputPath(path);
				btn.setText(data.getInputFileNameUiString());
				UpdateConfigContent();
			}
		});
	}

	void AddExpectedBtn(TableItem item, TestcaseData data) {
		TableEditor tbEdit = new TableEditor(uiTableTestcase);
		tbEdit.grabHorizontal = true;

		Button btn = new Button(uiTableTestcase, SWT.PUSH);
		btn.setText(data.getExpectedFileNameUiString());
		tbEdit.setEditor(btn, item, TABLE_COL_INDEX_EXPECTED);
		item.setData("col4", btn);
		btn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String path = getFilePath("*.csv");
				data.setExpectedPath(path);
				btn.setText(data.getExpectedFileNameUiString());
				UpdateConfigContent();
			}
		});
	}

	void AddPlusBtn() {
		TableItem item = new TableItem(uiTableTestcase, SWT.NONE);
		TableEditor tbBtnAdd = new TableEditor(uiTableTestcase);
		tbBtnAdd.grabHorizontal = true;
		Button btnAdd = new Button(uiTableTestcase, SWT.PUSH);
		btnAdd.setText("+");
		tbBtnAdd.setEditor(btnAdd, item, TABLE_COL_INDEX_NUM);

		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TestcaseData data = new TestcaseData();
				data.setName("Enter the test case name.");
				data.setMsimPath("");
				data.setInputPath("");
				data.setExpectedPath("");
				vTcData.add(data);
				UpdateUiList();
			}
		});
		item.addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				btnAdd.dispose();
			}
		});
	}

	void SetResultItem(int i, TC_RESULT r) {
		TableItem item = uiTableTestcase.getItem(i);
		TestcaseData data = vTcData.get(i);
		data.setResult(r);
		item.setText(TABLE_COL_INDEX_RESULT, data.getResult().toString());
		item.setForeground(TABLE_COL_INDEX_RESULT, arrColor[data.getResult().toInt()]);
	}

	void ResetResultItem() {
		for (int i = 0; i < vTcData.size(); i++) {
			SetResultItem(i, TC_RESULT.NONE);
		}

	}

	String getFilePath(String ext) {
		FileDialog dialog = new FileDialog(composite.getShell(), SWT.OPEN);
		dialog.setText("Open File");
		String[] filterExt = { ext, "*.*" };
		dialog.setFilterExtensions(filterExt);
		String ret = dialog.open();
		if (ret != null) {
			return ret;
		} else {
			return "";
		}
	}

	public static void RunMasterSim() {

		if (uiBtnRun == null)
			return;

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				uiBtnRun.notifyListeners(SWT.Selection, null);
			}
		});
	}
}
