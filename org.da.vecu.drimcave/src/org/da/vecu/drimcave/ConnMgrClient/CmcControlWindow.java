package org.da.vecu.drimcave.ConnMgrClient;

import org.da.vecu.drimcave.Util.AvtEnvIniParser;
import org.da.vecu.drimcave.Util.CommandExecutor;
import org.da.vecu.drimcave.server.ConnectionMgr;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class CmcControlWindow extends ApplicationWindow {
	Shell parentShell;
	public CmcControlWindow(Shell parentShell) {
		super(parentShell);
		this.parentShell = parentShell;
		
	}

	protected Control createContents(Composite parent) {
		getShell().setText("ConnMgrClient Controller");
		parent.setSize(295, 200);
		Point loc = parentShell.getLocation();
		parent.setLocation(loc.x + 100, loc.y + 100);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;
		parent.setLayout(layout);
		
        // Color backgroundColor = parentShell.getDisplay().getSystemColor(SWT.COLOR_BLUE);
        // parent.setBackground(backgroundColor);
		
        Control[] children = parent.getChildren();
        for (Control child : children) {
        	child.dispose();
        }

		GridData gdBtn = new GridData(GridData.FILL_BOTH);
		gdBtn.widthHint = 95;
		Button uiBtnCmcStart = new Button(parent, SWT.NONE);
		Button uiBtnCmcClose = new Button(parent, SWT.NONE);

		Button uiBtnCanoeOpen = new Button(parent, SWT.NONE);
		Button uiBtnCanoeStartstop = new Button(parent, SWT.NONE);
        
		Button uiBtnMatlabOpen = new Button(parent, SWT.NONE);
		
		uiBtnCmcStart.setLayoutData(gdBtn);
		uiBtnCmcStart.setText("Start");
		uiBtnCmcStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				connMgrClientStart();
				uiBtnCmcStart.setEnabled(false);
				uiBtnCmcClose.setEnabled(true);
				uiBtnCanoeOpen.setEnabled(true);
				uiBtnMatlabOpen.setEnabled(true);
			}
		});

		
		uiBtnCmcClose.setLayoutData(gdBtn);
		uiBtnCmcClose.setText("Close");
		uiBtnCmcClose.setEnabled(false);
		uiBtnCmcClose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				connMgrClientClose();
				uiBtnCmcClose.setEnabled(false);
				uiBtnCanoeOpen.setEnabled(false);
				uiBtnCanoeStartstop.setEnabled(false);
				uiBtnCmcStart.setEnabled(true);
				uiBtnMatlabOpen.setEnabled(false);
			}
		});
		
		
		uiBtnCanoeOpen.setLayoutData(gdBtn);
		uiBtnCanoeOpen.setText("Open Canoe");
		uiBtnCanoeOpen.setEnabled(false);
		uiBtnCanoeOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canoeOpen();
				uiBtnCanoeStartstop.setEnabled(true);
			}
		});

		uiBtnCanoeStartstop.setLayoutData(gdBtn);
		uiBtnCanoeStartstop.setText("Start Canoe");
		uiBtnCanoeStartstop.setEnabled(false);
		uiBtnCanoeStartstop.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				canoeStartStop(uiBtnCanoeStartstop);
			}
		});
		
		uiBtnMatlabOpen.setLayoutData(gdBtn);
		uiBtnMatlabOpen.setText("Open Matlab");
		uiBtnMatlabOpen.setEnabled(false);
		uiBtnMatlabOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				MatlabOpen();
				uiBtnMatlabOpen.setEnabled(true);
			}
		});

		return parent;
	}

	class ConnMgrClientExeThread extends Thread {
		public ConnMgrClientExeThread() {
		}

		public void run() {
			String strSuccess = "Run ConnectionMgr Client.";
			String strFail = "Failed to Run ConnectionMgr Client.";
			String log = String.format("avt sh path[%s]", AvtEnvIniParser.GetIniConnMgrClientPath());
			ViewManager.Print2(PrintType.Debug, log);
			
			if (CommandExecutor.RunConnMgrClientSh()) {
				ViewManager.Print2(PrintType.Info, strSuccess);
			} else {
				ViewManager.Print2(PrintType.Error, strFail);
			}
		}

		
	}
	void connMgrClientStart()
	{
		ConnMgrClientExeThread t = new ConnMgrClientExeThread();
		t.start();
	}
	
	void connMgrClientClose()
	{
		ConnectionMgr.send2ConnMgrClient_Close();
	}
	
	void canoeOpen()
	{
		ConnectionMgr.send2ConnMgrClient_CanoeOpen();
	}
	
	void MatlabOpen()
	{
		ConnectionMgr.send2ConnMgrClient_MatlabOpen();
	}
	
	boolean isStartCanoe = false;
	void canoeStartStop(Button btn)
	{
		//MessageDialog.openInformation(parentShell, "w", "window test");
		if(isStartCanoe)
		{
			isStartCanoe = false;
			ConnectionMgr.send2ConnMgrClient_CanoeStop();
			btn.setText("Start Canoe");
		}
		else
		{
			isStartCanoe = true;
			ConnectionMgr.send2ConnMgrClient_CanoeStart();
			btn.setText("Stop Canoe");
		}
	}

}
