package org.da.vecu.drimcave.Util;

public class WrapperBoolean {

	public boolean value;
	public String text;
	public WrapperBoolean()
	{
		this.value = false;
	}
	public WrapperBoolean(boolean b)
	{
		this.value = b;
	}
}
