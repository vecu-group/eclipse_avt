package org.da.vecu.drimcave.Util;

import org.eclipse.ui.PlatformUI;

public class ListenerMgr {
	private static boolean isActivate = false;
	private static AvtWindowListener listener;
	public static void ActivateListener()
	{
		if(isActivate)
			return;
		listener = new AvtWindowListener();
		//IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		PlatformUI.getWorkbench().addWindowListener(listener);
	}
}
