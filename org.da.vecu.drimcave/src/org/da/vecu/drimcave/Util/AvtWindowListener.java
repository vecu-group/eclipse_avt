package org.da.vecu.drimcave.Util;

import org.da.vecu.drimcave.server.ServerConnectionThread;
import org.da.vecu.drimcave.toolbar.TestBtn;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchWindow;

public class AvtWindowListener implements IWindowListener{

	@Override
	public void windowActivated(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		ServerConnectionThread.Close();
		TestBtn.TestServerClose();
	}

	@Override
	public void windowOpened(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		
	}

}
