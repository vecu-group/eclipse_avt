package org.da.vecu.drimcave.Util;

import java.util.Vector;

import org.da.vecu.drimcave.editors.TestcaseData;

public class ConfigParser {
	public static Vector<TestcaseData> parse(String text)
	{
		Vector<TestcaseData> vTc = new Vector<TestcaseData>();
		String[] tcData = text.split(";");
		for(int i = 0 ; i < tcData.length ; i++)
		{
			TestcaseData item = new TestcaseData();
			String[] tok = tcData[i].split(",");
			if(tok.length != 4)
				continue;
			item.setName(tok[0].trim());
			item.setMsimPath(tok[1].trim());
			item.setInputPath(tok[2].trim());
			item.setExpectedPath(tok[3].trim());
			vTc.add(item);
		}
		return vTc;
	}
	
	public static String exportText(Vector<TestcaseData> vTc)
	{
		String newline = CommandExecutor.getNewLine();
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < vTc.size() ; i++)
		{
			TestcaseData data = vTc.get(i);
			sb.append(data.getName());
			sb.append(",");
			sb.append(data.getMsimPath());
			sb.append(",");
			sb.append(data.getInputPath());
			sb.append(",");
			sb.append(data.getExpectedPath());
			sb.append(";");
			sb.append(newline);
		}
		return sb.toString();
	}
}
