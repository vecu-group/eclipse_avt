package org.da.vecu.drimcave.Util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import org.da.vecu.drimcave.editors.TestcaseData;
import org.da.vecu.drimcave.editors.TestcaseData.TC_RESULT;
import org.da.vecu.drimcave.view.ViewManager;
import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;

public class MasterSimRunner extends Thread {
	public static boolean isRun = false;
	private final int SUCCESS = -1;

	public interface MasterSimRunnerCallback {
		void FailedValidFile(int i);

		void SuccessValidFile();

		void FailedInputFile(int i);

		void SuccessInputFile();

		void StepExeTestcase(int i, TC_RESULT r);

		void FailedExeTestcase(int i);

		void SuccessExeTestcase();
	}

	Vector<TestcaseData> vTcData;
	MasterSimRunnerCallback callback;

	public MasterSimRunner(Vector<TestcaseData> vTc, MasterSimRunnerCallback callback) {
		vTcData = vTc;
		this.callback = callback;
		isRun = false;
	}

	public void run() {
		isRun = true;
		int ret = CheckValidFile();
		if (ret != SUCCESS) {
			callback.FailedValidFile(ret);
			isRun = false;
			return;
		}
		callback.SuccessValidFile();

		ret = MergeInputMsimFile();
		if (ret != SUCCESS) {
			callback.FailedInputFile(ret);
			isRun = false;
			return;
		}
		callback.SuccessInputFile();

		ret = ExeTestcase();
		if (ret != SUCCESS) {
			callback.FailedExeTestcase(ret);
			isRun = false;
			return;
		}

		callback.SuccessExeTestcase();
		isRun = false;
	}

	int CheckValidFile() {
		String strFail = "[Testcase] An error occurred while checking msim valid. Check MasterSimulator installation and version.";
		for (int i = 0; i < vTcData.size(); i++) {
			if (isRun == false)
				break;
			TestcaseData data = vTcData.get(i);
			if (CommandExecutor.RunCheckValidMsim(data.getMsimPath()) == false) {
				ViewManager.Print2(PrintType.Error, strFail);
				return i;
			}
		}

		return SUCCESS;
	}

	int MergeInputMsimFile() {
		for (int i = 0; i < vTcData.size(); i++) {
			if (isRun == false)
				break;
			TestcaseData data = vTcData.get(i);
			if (CreateMsimInputFile(data) == false)
				return i;
		}
		return SUCCESS;
	}

	boolean CreateMsimInputFile(TestcaseData data) {
		File fMsim = new File(data.getMsimPath());
		File fInput = new File(data.getInputPath());
		File fMsimInput = new File(data.getMsimInputPath());		
		if (fMsim.exists() == false)
			return false;
		if (fInput.exists() == false)
			return true;

		try {
			FileReader frMsim = new FileReader(fMsim);
			FileReader frInput = new FileReader(fInput);
			FileWriter fw = new FileWriter(fMsimInput, false);
			
			@SuppressWarnings("resource")
			BufferedReader brMsim = new BufferedReader(frMsim);
			@SuppressWarnings("resource")
			BufferedReader brInput = new BufferedReader(frInput);
			@SuppressWarnings("resource")
			BufferedWriter bw = new BufferedWriter(fw);

			String line = "";

			while ((line = brMsim.readLine()) != null) {
				bw.write(line);
				bw.newLine();
			}
			
			bw.newLine();
			bw.newLine();
			while ((line = brInput.readLine()) != null) {
				String tok[] = line.split(",");
				if(tok.length != 3)
					return false;
				bw.write(String.format("parameter %s.%s %s", tok[0].trim(),tok[1].trim(),tok[2].trim()));
				bw.newLine();
			}
			bw.newLine();
			
			brMsim.close();
			frMsim.close();

			brInput.close();
			frInput.close();
			
			bw.close();
			fw.close();

		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	int ExeTestcase() {
		String strSuccessForm = "[Testcase] The simulation is complete.[%s]";
		String strFail = "[Testcase] MasterSimulator is not ready. Make sure it is installed.";
		
		for (int i = 0; i < vTcData.size(); i++) {
			if (isRun == false)
				break;
			TestcaseData data = vTcData.get(i);
			WrapperBoolean result = new WrapperBoolean();
			if (CommandExecutor.RunSimulation(data.getMsimPath(), data.getExpectedPath(), i,result) == false) {
				ViewManager.Print2(PrintType.Error, strFail);
				return i;
			}
			ViewManager.Print2(PrintType.Info, String.format(strSuccessForm, data.getMsimPath()));
			TC_RESULT tcResult = getResult(result);
			callback.StepExeTestcase(i, tcResult);
			//if(tcResult == TC_RESULT.PASS)
			//{
			ViewManager.Print2(PrintType.Info,result.text);
			//}
		}
		return SUCCESS;
	}

	TC_RESULT getResult(WrapperBoolean result) {
		if(result.value == true)
		{
			return TC_RESULT.PASS;
		}
		else
		{
			return TC_RESULT.FAIL;			
		}
	}

	public void StopThread() {
		isRun = false;
		CommandExecutor.StopProcess();
	}
}
