package org.da.vecu.drimcave.Util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.List;

public class CommandExecutor {

	enum OsType {
		Windows, Linux, None
	}

	private static OsType OSTYPE = OsType.None;
	private static String NEWLINE = "\n";
	private static String TestMSPath = "D:\\Program Files\\IBK\\MasterSimulator 0.9\\";

	public static String getNewLine() {
		if (OSTYPE == OsType.None)
			InitOsType();
		return NEWLINE;
	}

	private static boolean isRun = false;
	private static Process process;

	private static String Run(List<String> cmd) throws IOException, InterruptedException {
		String s = null;
		StringBuilder stringbuilder = new StringBuilder();
		ProcessBuilder pb = new ProcessBuilder(cmd);

		pb.directory(new File(Paths.get("").toAbsolutePath().toString()));

		isRun = true;
		process = pb.start();

		BufferedReader stdOut = new BufferedReader(new InputStreamReader(process.getInputStream()));
		BufferedWriter stdIn = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));

		stdIn.newLine();
		// stdIn.flush();
		while ((s = stdOut.readLine()) != null) {
			stringbuilder.append(s).append(NEWLINE);
		}
		stringbuilder.append(NEWLINE);

		process.waitFor();
		isRun = false;
		return stringbuilder.toString();
	}

	public static void StopProcess() {
		if (isRun) {
			isRun = false;
			process.destroy();
		}
	}

	private static void InitOsType() {
		String os = System.getProperty("os.name").toLowerCase();

		OSTYPE = OsType.None;
		if (os.equals("windows 11") || os.equals("windows 10")) {
			OSTYPE = OsType.Windows;
			NEWLINE = "\r\n";
		} else if (os.equals("linux")) {
			OSTYPE = OsType.Linux;
		} else {
		}
	}

	public static boolean RunMasterSimUI() {
		String strMastersim = "";
		String s = null;
		if (OSTYPE == OsType.None)
			InitOsType();
		if (OSTYPE == OsType.Linux) {
			strMastersim = "MasterSimulatorUI";
		} else {
			strMastersim = TestMSPath + "MasterSimulatorUI.exe";
		}

		List<String> command = List.of(strMastersim);
		try {
			s = Run(command);
			FileLogMgr.Logging("MasterSimulatorUI",s);
		} catch (Exception e) {
			isRun = false;
			return false;
		}
		return true;
	}

	public static boolean RunCheckMasterSim() {
		String strMastersim = "";
		if (OSTYPE == OsType.None)
			InitOsType();
		if (OSTYPE == OsType.Linux) {
			strMastersim = "MasterSimulator";
		} else {
			strMastersim = TestMSPath + "MasterSimulator.exe";
		}

		List<String> command = List.of(strMastersim);
		try {
			Run(command);
		} catch (Exception e) {
			isRun = false;
			return false;
		}
		return true;
	}

	public static boolean RunCheckValidMsim(String path) {
		String strMastersim = "";
		String s = null;
		if (OSTYPE == OsType.None)
			InitOsType();

		if (OSTYPE == OsType.Linux) {
			strMastersim = "MasterSimulator";
		} else {
			strMastersim = TestMSPath + "MasterSimulator.exe";
			return true;
		}

		List<String> command = List.of(strMastersim, "--checkmsim", path);
		try {
			s = Run(command);
		} catch (Exception e) {
			isRun = false;
			return false;
		}

		if (s == null) {
			return false;
		}
		int findIdx = s.indexOf("[AVT] check msim success");
		if (findIdx < 0) {
			return false;
		}
		return true;
	}

	public static boolean RunSimulation(String msimPath, String expectedPath, int indexExpected,
			WrapperBoolean result) {
		// MasterSimulator --verbosity-level=1 --testcase=1 ".msim path" ".input path"
		String strMastersim = "";
		String s = null;
		if (OSTYPE == OsType.None)
			InitOsType();

		if (OSTYPE == OsType.Linux) {
			strMastersim = "MasterSimulator";
		} else {
			strMastersim = TestMSPath + "MasterSimulator.exe";
		}
		List<String> command = List.of(strMastersim, "--verbosity-level=3",
				String.format("--testcase=%d", indexExpected + 1), msimPath, expectedPath);
		try {
			s = Run(command);
			FileLogMgr.Logging("MasterSimulator",s);
		} catch (Exception e) {
			isRun = false;
			return false;
		}

		if (s == null) {
			return false;
		}

		int findIdx = s.indexOf("[AVT] testcase pass");
		result.text = s;
		if (findIdx < 0) {
			result.value = false;
		} else {
			result.value = true;
		}
		return true;

	}

	public static boolean RunConnMgrClientSh()
	{
		String s = null;
		if (OSTYPE == OsType.None)
			InitOsType();

		List<String> command = List.of(AvtEnvIniParser.GetIniConnMgrClientPath());
		try {
			s = Run(command);
			//FileLogMgr.Logging("MasterSimulatorUI",s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	
	static boolean init = false;

	public static boolean InitChRun() {
		if (init)
			return true;
		init = true;
		if (Run2(List.of("modprobe", "can_raw")) == false) {
			return false;
		}
		if (Run2(List.of("modprobe", "can")) == false) {
			return false;
		}
		if (Run2(List.of("modprobe", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "add", "dev", "vcan0", "type", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "add", "dev", "vcan1", "type", "vcan")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan0", "up")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan1", "up")) == false) {
			return false;
		}
		if (Run2(List.of("ip", "link", "set", "vcan0", "mtu", "72")) == false) {
			return false;
		}
		return true;
	}

	private static boolean Run2(List<String> cmd) {
		ProcessBuilder pb = new ProcessBuilder(cmd);
		pb.directory(new File(Paths.get("").toAbsolutePath().toString()));

		try {
			Process process = pb.start();
			process.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
