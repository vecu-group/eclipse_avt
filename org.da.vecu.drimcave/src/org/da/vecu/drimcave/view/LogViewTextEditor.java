
package org.da.vecu.drimcave.view;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;

public class LogViewTextEditor {
	public enum PrintType {
		Log("[Log] "),
		Info("[Info] "),
		Error("[Error] "),
		Warning("[Warning] "),
		Debug("[Debug] "),
		None("[None] ");

		private final String value;

		private PrintType(String value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return value;
		}
	}

	private StyledText textUi;
	public static final String ID = "org.da.vecu.drimcave.partdescriptor.log";

	@Inject
	public LogViewTextEditor() {

	}

	@PostConstruct
	public void postConstruct(Composite parent) {
		textUi = new StyledText(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
		textUi.setEditable(false);
	}

	public void Print(PrintType type, String s) {
		if (type == PrintType.Debug && !ViewManager.DEBUG)
			return;

		String p = String.format("%s%s\n", type.toString(), s);
		textUi.append(p);
		SetLastCursor();
	}

	public void SetLastCursor() {
		textUi.setSelection(textUi.getCharCount() - 1);
	}

	public void setFocus() {
		textUi.setFocus();
	}

}