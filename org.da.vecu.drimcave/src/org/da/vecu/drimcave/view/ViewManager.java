package org.da.vecu.drimcave.view;

import org.da.vecu.drimcave.view.LogViewTextEditor.PrintType;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class ViewManager {
	public static boolean DEBUG = true;
/*
	private static LogViewTextEditor getSconsLogViewInstance() {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			IViewPart viewPart = activePage.findView(LogViewTextEditor.ID);
			LogViewTextEditor view = viewPart.getAdapter(LogViewTextEditor.class);
			return view;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void showSconsLogView() {
		try {
			IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			activePage.showView(LogViewTextEditor.ID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
	public static void Print(PrintType type, String s) {
		IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewPart viewPart = activePage.findView(LogViewTextEditor.ID);
		if (viewPart == null) {
			try {
				activePage.showView(LogViewTextEditor.ID);
				viewPart = activePage.findView(LogViewTextEditor.ID);
			} catch (PartInitException e) {
				e.printStackTrace();
				return;
			}
		}
		LogViewTextEditor view = viewPart.getAdapter(LogViewTextEditor.class);
		if (view == null)
			return;
		view.Print(type, s);
	}

	public static void Print2(PrintType type, String s) {
	   	 Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				Print(type,s);				
			}
	   	 });
	}
}
