* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 Eclipse 통합 개발 환경을 위한 것으로 Eclipse 플러그인으로 개발한 AVT에서 Testcase 구성과 시뮬레이션 환경을 제공하기 위한 용도로 활용됩니다."""
* * *

# README

## 개요

Eclipse Plugin으로 개발한 AVT는  Testcase 구성과 시뮬레이션 환경을 제공한다.

## 개발 환경

Eclipse IDE for Embedded C/C++ Developers  (version 2022-06)

open JDK : 17.0.9

## 주요 기능

- config file(.dccfg) 생성 및 편집 기능
    
    ![Untitled](doc/Untitled.png)
    
    ![Untitled](doc/Untitled%201.png)
    
    ![Untitled](doc/Untitled%202.png)
    
    1. Testcase 추가
    2. Testcase Name
    3. 시뮬레이션 file 등록
    4. Testcase 실행(시뮬레이션 실행)
    
- MasterSimulator UI 연동
    
    ![Untitled](doc/Untitled%203.png)
    
- Connection Manager
    
    ![Untitled](doc/Untitled%204.png)
    
- Mastersim 시뮬레이션 실행(Testcase) & Log View
    
    ![Untitled](doc/Untitled%205.png)
